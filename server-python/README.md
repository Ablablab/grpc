# Server in Python

## Setup
### Configurazione Ambiente Python
```
./setup.sh
```
installa tramite pip grpc e protobuf.

### HelloWorld
Prima di avviare l'HelloWorld in Python, è necessario avviare l'helloworld in cpp [agent-cpp](../agent-cpp).
```
cd netserver
./compileproto.sh
```
In questo modo i file .proto verranno compilati anche per python. Infine:  
```
python greeter_client
```

Il server, dalla vista di grpc, assumerà infatti il ruolo di client (in quanto richiede di eseguire all'agente dei comandi).

Se l'helloworld ha funzionato in entrambe le sue componenti, verrà restituito un messaggio di "Hello".

### Esecuzione Server
```
./run.sh
```
Compilerà i file .proto del progetto e avvierà il server python. 

## Tutorial e Riferimenti
E' importante https://grpc.io/docs/tutorials/basic/python.html

### Requisiti Server
#### Funzionali
- Il sistema deve permettere il monitoraggio di eventi e configurazioni di nodi tramite un singolo centro.
- Il server deve rappresentare il controller dell'intera rete.
- Le informazioni ricevute dagli agent devono essere aggiornate periodicamente (polling)
- * Opzionale: La comunicazione deve essere di tipo real time, continua.
#### Non Funzionali
- La comunicazione con gli agent deve avvenire tramite GPRC.
- * Opzionale:  Il sistema deve interfacciarsi con i servizi Grafana (https://grafana.com/)
- Il server deve poter essere adattato facilmente ad altre piattaforme o librerie per la gestione dei log, è ragionevole l'uso di un linguaggio ad alto livello come Python.
