import grpc

import netagent_pb2
import netagent_pb2_grpc
import datetime


def run():
    print "Starting netserver"
    # read in certificate
    netserver_key_file = open('../../auth/netserver.key')
    netserver_key = netserver_key_file.read()
    netserver_key_file.close()

    netserver_crt_file = open('../../auth/netserver.crt')
    netserver_crt = netserver_crt_file.read()
    netserver_crt_file.close()

    ca_crt_file = open('../../auth/ca.crt')
    ca_crt = ca_crt_file.read()
    ca_crt_file.close()
    creds = grpc.ssl_channel_credentials(root_certificates=ca_crt, private_key=netserver_key, certificate_chain=netserver_crt)
    channel = grpc.secure_channel('localhost:50051', creds, options=(('grpc.ssl_target_name_override', "localhost",),))

    stub = netagent_pb2_grpc.NetToolStub(channel)

    #print execute_command_remotely(stub, "ls /")
    response = add_port_scan_listener(stub)

    receive_events_stream(stub)

def execute_command_remotely(stub, command):
    print "Asking for command..."
    request = netagent_pb2.ExecuteCommandRequest()
    request.command = command
    response = stub.ExecuteCommand(request)
    return response.result

def check_net_info(stub):
    print "Asking for net info..."
    response = stub.CheckNetInfo(netagent_pb2.NetInfoRequest())
    return response.ifconfig , response.netstat

def receive_events_stream(stub):
    response = stub.StreamEvents(netagent_pb2.StreamEventRequest())
    try:
        for r in response:
            rtimestamp = datetime.datetime.utcfromtimestamp(r.timestamp).strftime('%Y-%m-%dT%H:%M:%SZ')
            print "event {}, result is {} from node {} at time {}".format(r.command_id, r.result, r.src, rtimestamp )
    except grpc._channel._Rendezvous as err:
        print(err)

def add_port_scan_listener(stub):
    event_id = 1
    command = "nstat -z | grep 'TcpOutRsts' | awk '{ print $2 }'"
    polling_interval_milli = 1000
    comparator = 1
    expected_value = "100"
    absolute = True
    result_type_decimal = True

    add_event_listener(stub, event_id, command, polling_interval_milli, comparator, expected_value, absolute, result_type_decimal)

def add_event_listener(stub, event_id, command, polling_interval_milli, comparator, expected_value, absolute, result_type_decimal):
    request = netagent_pb2.EventListenerRequest()
    request.event_id = event_id
    request.command = command
    request.polling_interval = polling_interval_milli
    request.comparator=comparator
    request.expected_value = expected_value
    request.absolute = absolute
    request.result_type_decimal = result_type_decimal

    response = stub.AddEventListener(request)


if __name__ == '__main__':
    run()
