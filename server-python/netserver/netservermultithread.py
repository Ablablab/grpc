#import grpc
#import netagent_pb2
#import netagent_pb2_grpc
import argparse
import socket
import grpc
from Configurator import Configurator
from EventQueue import EventQueue,get_from_queue,put_in_queue
from threading import Thread
from sys import exit
import time
import logging
from entity.dao import init_db, add_event, get_conf_event_by_id
from entity.ResultEvent import ResultEvent
from datetime import datetime
from lib.PySettings.SettingsManager import SettingsManager
from mocks.ConfigurationMock import ConfigurationMock, Agent
from mocks.ListenerMock import ListenerMock
from netserver import netagent_pb2
from netserver import netagent_pb2_grpc
import json

OVERALL_INFO = "/tmp/overall_info.json"
listener_mock = ListenerMock()
conf = Configurator()
settings = SettingsManager.getSettings()
events = []
configuration_mock = ConfigurationMock()
agents = []

def in_db():
    init_db()
    conf.get_events_from_json()
    conf.feed_data_source()

def configure():

    event_ids = settings.get_event_ids()
    working = 0

    for e in event_ids:
        ev_to_add = get_conf_event_by_id(e)
        if ev_to_add is not None:
            events.append(ev_to_add)
            working += 1
        else:
            logging.warning("Bad configuration : id {} not found ".format(e))

    if not (working > 0):
        logging.warning("Nothing to monitor. Good Bye!")
        exit()

    mininet_clients = settings.get_mininet_clients()

    #fill agents
    with open(OVERALL_INFO) as overall_info:
        overall_json = json.load(overall_info)

    for m in mininet_clients:
        if m in overall_json:
            mgt_ip = overall_json[m]["mgt_IP"]
            if ping_check(mgt_ip):
                agents.append(Agent(ip=mgt_ip, port=settings.get_default_client_port()))
        else:
            logging.warning("{} not in json".format(m))

    for a in agents:
        print a

def run():

    q = EventQueue()
    id_counter = 0

    # agents = ConfigurationMock.gen_agents()
    #agents = []
    #agents.append(Agent(ip="localhost", port="50051"))

    for a in agents:

        create_listener(id=id_counter, queue=q, agent=a)
        id_counter += 1

    create_logger(id_counter, q)
    q.join()


def ping_check(host):
    import os
    response = os.system("ping -c 1 -t 1 " + host)

    #and then check the response...
    return response == 0

def get_auth():
    netserver_key_file = open(settings.get_netserver_key())
    netserver_key = netserver_key_file.read()
    netserver_key_file.close()

    netserver_crt_file = open(settings.get_netserver_crt())
    netserver_crt = netserver_crt_file.read()
    netserver_crt_file.close()

    ca_crt_file = open(settings.get_ca_crt())
    ca_crt = ca_crt_file.read()
    ca_crt_file.close()

    return grpc.ssl_channel_credentials(root_certificates=ca_crt, private_key=netserver_key, certificate_chain=netserver_crt)


def listen(id, queue, agent):

        logging.info("This listener is referred to {}:{} client".format(agent.ip, agent.port))

        # print "Sending Grpc to client for :\n{}".format(e)

        channel = grpc.secure_channel('{}:{}'.format(agent.ip, agent.port), get_auth(),options=(('grpc.ssl_target_name_override', "localhost",),))

        stub = netagent_pb2_grpc.NetToolStub(channel)

        stop = False

        try:
            for e in events:
                send_event(stub, event_id=e.id, command=e.command, polling_interval_milli=e.polling,
                                   comparator=e.comparator, expected_value=e.expected_res, absolute=e.abs,
                                   result_type_decimal=e.result_type_decimal)
        except grpc._channel._Rendezvous as err:
            #print(err)
            logging.info("Rendezvous exception, client not available. Exit..")
            stop = True

        while not stop:
            response = stub.StreamEvents(netagent_pb2.StreamEventRequest())
            try:
                for r in response:
                    rtimestamp = datetime.utcfromtimestamp(r.timestamp).strftime('%Y-%m-%d %H:%M:%S')

                    res_ev = ResultEvent(src=r.src, command_id=r.command_id, result=r.result, timestamp=rtimestamp)
                    # put_in_queue(listener_mock.get_event(), queue)
                    put_in_queue(res_ev, queue)
                    # print "event {}, result is {} from node {} at time {}".format(r.command_id, r.result, r.src, rtimestamp )
            except grpc._channel._Rendezvous as err:
                #print(err)
                logging.info("Rendezvous exception, client not available. Exit..")
                stop = True
            # listener_mock.gen_event()

            # time.sleep(id + 2)
        queue.task_done()


def log(id, queue):
    while True:
        res = get_from_queue(queue)
        for r in res:
            logging.info(r)
            add_event(r)
        time.sleep(id + 2)

    queue.task_done()


def create_listener(id, queue, agent):
    worker = Thread(target=listen, args=(id, queue, agent,))
    worker.setDaemon(False)
    worker.start()


def create_logger(id, queue):
    worker = Thread(target=log, args=(id, queue,))
    worker.setDaemon(False)
    worker.start()


def receive_events_stream(stub):
    response = stub.StreamEvents(netagent_pb2.StreamEventRequest())
    try:
        for r in response:
            rtimestamp = datetime.utcfromtimestamp(r.timestamp).strftime('%Y-%m-%dT%H:%M:%SZ')

            # print "event {}, result is {} from node {} at time {}".format(r.command_id, r.result, r.src, rtimestamp )
    except grpc._channel._Rendezvous as err:
        raise grpc._channel._Rendezvous


def send_event(stub, event_id, command, polling_interval_milli, comparator, expected_value, absolute, result_type_decimal):
    request = netagent_pb2.EventListenerRequest()
    request.event_id = event_id
    request.command = command
    request.polling_interval = polling_interval_milli
    request.comparator=comparator
    request.expected_value = expected_value
    request.absolute = absolute
    request.result_type_decimal = result_type_decimal

    try:
        response = stub.AddEventListener(request)
    except grpc._channel._Rendezvous as err:
        print(err)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='GRPC server')
    parser.add_argument('-init', help='Init DB and populate it with configuration events', required=False, default=False, action="store_true")
    parser.add_argument('-run', help='Run server', required=False, default=False, action="store_true")

    opts = parser.parse_args()

    logging.basicConfig(filename='grpc_server.log',level=logging.DEBUG)

    if opts.init:
        # 1)
        logging.debug("DB initialization")
        in_db()
    if opts.run:
        # 2)
        logging.debug("Configuring events")
        configure()
        logging.debug("The application is running")
        run()
