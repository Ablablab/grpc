# System modules
from Queue import Queue
from threading import Lock


class EventQueue:

    def __init__(self):
        self.queue = Queue()
        self.counter = 0
        self.lock = Lock()

    def add_counter(self):
        self.lock.acquire()
        try:
            self.counter = self.counter + 1
        finally:
            self.lock.release()

    def get_counter(self):
        return self.counter

    def get(self):
        return self.queue.get()

    def put(self,event):
        self.queue.put(event)

    def task_done(self):
        self.queue.task_done()

    def join(self):
        self.queue.join()

    def empty(self):
        return self.queue.empty()


def put_in_queue(event, q):
    q.put(event)


def get_from_queue(q):
    res = []
    while not q.empty():
        event = q.get()
        res.append(event)
    return res
