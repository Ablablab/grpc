from random import randrange
from entity.Event import Event

min_port = 10000
max_port = 50000


class ConfigurationMock:

    def __init__(self):
        pass

    @staticmethod
    def gen_conf_event(id):
        command = "echo ciao"
        comparator = randrange(0, 9)
        expected_res = "ciao"
        abs = True
        polling = 30
        desc = "A command to greet people"

        return Event(id=id, command=command, comparator=comparator, expected_res=expected_res, abs=abs,
                     polling=polling, desc=desc)

    @staticmethod
    def gen_agents():
        num = randrange(1, 4)
        agents = []

        for n in xrange(num):
            agents.append(Agent())

        return agents


def get_random_ip():
    return "{}.{}.{}.{}".format(randrange(0, 255), randrange(0, 255), randrange(0, 255), randrange(0, 255))


def get_random_port():
    return randrange(min_port, max_port)


class Agent:

    def __init__(self, ip=get_random_ip(), port=get_random_port()):
        self.ip = ip
        self.port = port

    def __str__(self):
        return "IP : {} , port {}".format(self.ip, self.port)
