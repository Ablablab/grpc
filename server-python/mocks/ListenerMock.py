from datetime import datetime
from random import randrange
from entity.ResultEvent import ResultEvent


class ListenerMock:

    def __init__(self, event=None):
        self.event = event

    def gen_event(self):
        self.event = ResultEvent(src="{}.{}.{}.{}".format(randrange(0, 255), randrange(0, 255), randrange(0, 255),
                                                          randrange(0, 255)), timestamp=datetime.utcnow(),
                                 command_id=randrange(0, 8), result=str(randrange(1, 1000)))

    def get_event(self):
        return self.event
