from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker
from Base import Base
from datetime import datetime
from sqlalchemy import text
from Event import Event
from lib.PySettings.SettingsManager import SettingsManager


def get_engine():
    settings = SettingsManager.getSettings()
    db_name = settings.get_dbname()
    db_engine = settings.get_db_engine()
    db_user = settings.get_db_user()
    db_password = settings.get_db_password()
    db_ip = settings.get_db_ip()
    # to build database, execute this file

    #Not secure!!! warning injection!
    engine = create_engine(db_engine+'://'+db_user+':'+db_password+'@'+db_ip+'/'+db_name)

    return engine


def get_conf_event_by_id(id):
    """
    This method returns a configuration event, like an ARP Poisoning event to register it to the agent
    """
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()
    Session = sessionmaker(bind=eng)
    ses = Session()
    return ses.query(Event).filter(Event.id.like(id)).first()


def add_conf_event(event):
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()

    Session = sessionmaker(bind=eng)
    ses = Session()

    ses.merge(event)
    ses.commit()


def add_event(event):
    dbname = SettingsManager.getSettings().get_dbname()
    eng = get_engine()

    Session = sessionmaker(bind=eng)
    ses = Session()

    ses.add(event)
    ses.commit()


def init_db():
    engine = get_engine()
    Base.metadata.create_all(engine, checkfirst=True)
