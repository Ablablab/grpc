from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship
from Base import Base


class Event(Base):

    __tablename__ = "event"

    id = Column(Integer, primary_key=True, autoincrement=False)
    command = Column(String(64))
    comparator = Column(Integer)
    expected_res = Column(String(32))
    abs = Column(Boolean)
    polling = Column(Integer)
    desc = Column(String(32))
    result_type_decimal = Column(Boolean)

    def __str__(self):
        return "Event id : {} command : {} \
        comparator : {} expected_res : {} \
        abs : {} polling : {} desc : {} decimal : {}".format(self.id, self.command, self.comparator,
        self.expected_res, self.abs, self.polling, self.desc, self.result_type_decimal)
