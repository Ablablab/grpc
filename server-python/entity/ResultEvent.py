from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from Base import Base


class ResultEvent(Base):

    __tablename__ = "result_event"

    id = Column(Integer, primary_key=True, autoincrement=True)
    command_id = Column(Integer)
    timestamp = Column(DateTime)
    result = Column(String(32))
    src = Column(String(32))

    def __str__(self):
        return "Event id : {} timestamp : {} \
        command : {} \
        result : {} \
        src : {}".format(self.id, self.timestamp,
        self.command_id,self.result, self.src)
