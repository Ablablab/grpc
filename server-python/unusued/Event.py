class Event:

    def __init__(self, id, desc, command, comparator, expected_res, timestamp, result, abs=True, polling=30):
        self.id = id
        self.command = command
        self.comparator = comparator
        self.expected_res = expected_res
        self.abs = abs
        self.polling = polling
        self.desc = desc

    def __str__(self):
        return "Event id : {} command : {} \
        comparator : {} expected_res : {} \
        abs : {} polling : {} desc : {}".format(self.id, self.command, self.comparator,
        self.expected_res, self.abs, self.polling, self.desc)


class ResultEvent:

    def __init__(self,command_id, timestamp, result , src,id=0):
        self.id = id
        self.command_id = command_id
        self.timestamp = timestamp
        self.result = result
        self.src = src

    def __str__(self):
        return "Event id : {} timestamp : {} \
        command : {} \
        src : {} \
        result : {}".format(self.id,self.timestamp,self.command_id,
        self.src,
        self.result)
