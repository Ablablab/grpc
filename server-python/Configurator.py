from json import load
from entity.Event import Event
from entity.dao import add_conf_event

DIR = "/home/alessandro/git/grpc/server-python/"


class Configurator:

    def __init__(self, default_path="{}events.json".format(DIR)):
        self.default_path = default_path
        self.events = []

    def get_events_from_json(self):
        with open(self.default_path) as data_file:
            data = load(data_file)

        for d in data:
            id = d["id"]

            with open("{}{}".format(DIR, d["command"])) as cmd:
                command = cmd.read()

            comparator = d["comparator"]
            expected_res = d["expected_res"]

            if d["abs"] == 1:
                abs = True
            else:
                abs = False

            if d["result_type_decimal"] == 1:
                result_type_decimal = True
            else:
                result_type_decimal = False

            polling = d["polling"]
            desc = d["desc"]

            ev = Event(id=id, command=command, comparator=comparator, expected_res=expected_res, abs=abs, polling=polling,
                       desc=desc, result_type_decimal=result_type_decimal)
            self.events.append(ev)

    def feed_data_source(self):
        for e in self.events:
            add_conf_event(e)

    def print_events(self):
        for e in self.events:
            print e
