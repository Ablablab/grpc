
#ifndef SHARED_QUEUE_H
#define SHARED_QUEUE_H
#include <queue>
#include "netagent.pb.h"
#include <mutex>
#include <condition_variable>
using netagent::Event;

namespace Event_Shared_Queue{
  class Queue;
};
class Event_Shared_Queue::Queue
{

 public:
  Event pop();
  void push(Event& item);


 private:
  std::queue<Event> queue_;
  std::mutex mutex_;
  std::condition_variable cond_;

};


#endif
