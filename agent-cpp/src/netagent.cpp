/*
* Andrea Gennusa, Alessandro Fazio
*/

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <thread>
#include <memory>
#include <mutex>
#include <set>
#include <ctime>
#include <string>
#include <stdexcept>
#include <grpc++/grpc++.h>
#include <unistd.h>
#include "netagent.grpc.pb.h"
#include "shared_queue.h"
#include "iniReader.h"

// it's better define macros, in this way if a setting's call is wrong failure is on compiling. otherwise the failure could be at runtime
#define node_id "node_id"
#define address_grpc "address_grpc"
#define port_grpc "port_grpc"
#define command_netstat "command_netstat"
#define command_ifconfig "command_ifconfig"
#define agent_key "agent_key"
#define agent_crt "agent_crt"
#define ca_crt "ca_crt"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerWriter;
using grpc::Status;
using netagent::NotHelloRequest;
using netagent::NotHelloReply;
using netagent::NetInfoRequest;
using netagent::NetInfoReply;
using netagent::EventListenerRequest;
using netagent::EventListenerReply;
using netagent::EventListenerRemoveRequest;
using netagent::EventListenerRemoveReply;
using netagent::ExecuteCommandRequest;
using netagent::ExecuteCommandReply;
using netagent::StreamEventRequest;
using netagent::Event;
using netagent::NetTool;
using Event_Shared_Queue::Queue;


Queue shared_queue;
std::mutex mutex_listeners;
std::mutex mutex_listeners_to_quit;
std::set<int> set_listeners;
std::set<int> set_listeners_to_quit;

char const * config_file_relative_path = "settings.ini";











/*


Functions


*/




std::string read_text_file( const std::string& filename)
{
  std::ifstream file ( filename.c_str(), std::ios::in);
  std::stringstream ss;
  if ( file.is_open()){
    ss << file.rdbuf();
    file.close();
  }

  return ss.str();
}

int execute_command(char const * command, std::string& response)
// it returns error provided by command execution.
{
  FILE * stream;
  int max_buffer = 512;
  char buffer[max_buffer];
  stream = popen(command, "r");
  if (stream) {
    while (!feof(stream))
    if (fgets(buffer, max_buffer, stream) != NULL)
    //strcat(*response, buffer);
    response.append(buffer);

    return pclose(stream);
  } else{
    return 1;
  }
}

/**
* return true if server requested the stop of listener, false otherwise
*/
bool check_if_stopped(int event_id){
  std::unique_lock<std::mutex> mlock(mutex_listeners_to_quit);
  std::set<int>::iterator it = set_listeners_to_quit.find(event_id);
  if(it != set_listeners_to_quit.end()) {
    set_listeners_to_quit.erase(event_id);
    mlock.unlock();
    return true;
  }
  return false;
}

/*
server requests the end of listener execution
*/
void request_stop(int event_id){
  std::unique_lock<std::mutex> mlock(mutex_listeners_to_quit);
  set_listeners_to_quit.insert(event_id);
  mlock.unlock();
}


bool check_expected_result_numbers(double value_to_compare, double expected_value, int comparator){
  switch (comparator) {
    case 1:
    return value_to_compare > expected_value;
    case 2:
    return value_to_compare >= expected_value;
    case 3:
    return value_to_compare < expected_value;
    case 4:
    return value_to_compare <= expected_value;
    case 5:
    return value_to_compare == expected_value;
    case 6:
    return value_to_compare != expected_value;
    default:
    return false;
  }
}

bool check_expected_result_strings(std::string& value_to_compare, std::string& expected_value, int comparator){
  switch (comparator) {
    case 5:
    return value_to_compare.compare(expected_value);
    case 6:
    return !value_to_compare.compare(expected_value);
    default:
    return false;
  }
}

/*
send event to shared queue
*/
void save_event(int event_id, std::string command, std::string value, std::string expected_value){
  std::cout << "evento registrato: " << event_id << std::endl;
  netagent::Event event = netagent::Event();
  event.set_command_id(event_id);
  event.set_timestamp(std::time(nullptr));
  event.set_result(value);
  event.set_src(std::string(getOptionToString(node_id)));
  shared_queue.push(event);
}

/*
routine executed by listener threads
*/
void listener_routine(int event_id, int polling_interval_milli, std::string command, int comparator, std::string expected_value, bool absolute, bool result_type_decimal) {
  if (polling_interval_milli <= 0){
    polling_interval_milli = 1000;
  }

  std::string old("0");
  double to_compare_double = 0.0;
  while (true){
    if (check_if_stopped(event_id)){
      return;
    }
    netagent::Event event = netagent::Event();

    // executing command
    std::string command_result;
    execute_command(command.c_str(), command_result);

    try {
      // comparing numbers
      if (result_type_decimal){
        if (absolute){

          if (check_expected_result_numbers(std::stod(command_result), std::stod(expected_value), comparator) )
          save_event(event_id,command, command_result, expected_value);

        } else{
          to_compare_double = std::stod(old) - std::stod(command_result);

          old = command_result;
          if (check_expected_result_numbers(to_compare_double, std::stod(expected_value), comparator) )
          save_event(event_id,command, std::to_string(to_compare_double), expected_value);

        }
        // comparing strings
      } else {
        if (check_expected_result_strings(command_result, expected_value, comparator))
        save_event(event_id,command, command_result, expected_value);

      }
    } catch (...) {
      save_event(0,command, command_result, expected_value);
      std::cerr << "Error on command " << command << std::endl ;
    }


    usleep(polling_interval_milli * 1000);
  }
}
















/**
*
*  GRPC DATA
*
*
*/


// Logic and data behind the server's behavior.
class NetToolServiceImpl final : public NetTool::Service {
  Status SayNotHello(ServerContext* context, const NotHelloRequest* request,
    NotHelloReply* reply) override {
      std::string prefix("No No, Not Hello ");
      reply->set_message(prefix + request->name());
      return Status::OK;
    }

    Status CheckNetInfo(ServerContext* context, const NetInfoRequest* request,
      NetInfoReply* reply) override {
        std::string ifconfig;
        execute_command(getOptionToString(command_ifconfig).c_str(), ifconfig);
        reply->set_ifconfig(ifconfig);

        std::string netstat;
        execute_command(getOptionToString(command_netstat).c_str(), netstat);
        reply->set_netstat(netstat);

        return Status::OK;
      }

      Status StreamEvents(ServerContext* context, const StreamEventRequest* request,
        ServerWriter<netagent::Event>* reply) override {


          while (true) {
            // qui prendere eventi dalla coda e immetterli nella pipe di grpc.
            netagent::Event event = shared_queue.pop();

            reply->Write(event);
            //delete event; TODO: check if memory leak
          }
          return Status::OK;
        }

        Status AddEventListener(ServerContext* context, const EventListenerRequest* request,
          EventListenerReply* reply) override {


            std::unique_lock<std::mutex> mlock(mutex_listeners);

            std::set<int>::iterator it = set_listeners.find(request->event_id());
            if(it != set_listeners.end())
            std::cout<<"requested listener for " << request->event_id() << ", but it'a already working"<<std::endl;
            else {
              set_listeners.insert(request->event_id());
              std::cout<<"requested listener for " << request->event_id() << ", thread started"<<std::endl;
              std::thread listener(listener_routine, request->event_id(), request->polling_interval(), request->command(), request->comparator(), request->expected_value(), request->absolute(), request->result_type_decimal() );
              listener.detach();

            }
            mlock.unlock();
            return Status::OK;
          }

          Status RemoveEventListener(ServerContext* context, const EventListenerRemoveRequest* request,
            EventListenerRemoveReply* reply) override {

              request_stop(request->event_id());
              std::cout<<"requested stop for " << request->event_id() << std::endl;
              return Status::OK;
            }


            Status ExecuteCommand(ServerContext* context, const ExecuteCommandRequest* request,
              ExecuteCommandReply* reply) override {

                std::string result;
                execute_command(request->command().c_str(), result);
                reply->set_result(result);

                return Status::OK;
              }

            };










            /*


            main



            */



            void RunServer() {
              std::cout << "Net Agent is booting, loading authentication files..." << std::endl;
              parseIniFile(config_file_relative_path);

              std::string server_address(getOptionToString(address_grpc));
              NetToolServiceImpl service;

              std::string key = read_text_file(getOptionToString(agent_key));
              std::string cert = read_text_file(getOptionToString(agent_crt));
              std::string root = read_text_file(getOptionToString(ca_crt));

              ServerBuilder builder;

              grpc::SslServerCredentialsOptions::PemKeyCertPair key_cert = 	{key,cert};
              grpc::SslServerCredentialsOptions sslOps;
              sslOps.pem_root_certs = root;
              sslOps.pem_key_cert_pairs.push_back (key_cert);

              builder.AddListeningPort(server_address, grpc::SslServerCredentials( sslOps ));


              // Listen on the given address without any authentication mechanism.
              // builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
              // Register "service" as the instance through which we'll communicate with
              // clients. In this case it corresponds to an *synchronous* service.
              builder.RegisterService(&service);
              // Finally assemble the server.
              std::unique_ptr<Server> server(builder.BuildAndStart());
              std::cout << "Server listening on " << server_address << std::endl;

              // Wait for the server to shutdown. Note that some other thread must be
              // responsible for shutting down the server for this call to ever return.
              server->Wait();
            }

            int main(int argc, char** argv) {
              RunServer();

              return 0;
            }
