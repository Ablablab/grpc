// Andrea Gennusa, Alessandro Fazio

#include "shared_queue.h"




Event Event_Shared_Queue::Queue::pop()
{
  std::unique_lock<std::mutex> mlock(mutex_);
  while (queue_.empty())
  {
    cond_.wait(mlock);
  }
  auto val = queue_.front();
  queue_.pop();
  mlock.unlock();
  return val;
}

void Event_Shared_Queue::Queue::push(Event& item)
  {
    std::unique_lock<std::mutex> mlock(mutex_);
    queue_.push(item);
    mlock.unlock();
    cond_.notify_one();

  }
