# Client Agent in C++

## Setup
### Configurazione Ambiente per Client C++
Questo passaggio è fondamentale per riuscire a compilare codice in cpp. La compilazione di grpc impiegherà molto tempo (circa 10 minuti su un i7 7700K).
```
./setup_environment.sh
```
Eseguirà l'installazione dei pacchetti necessari, il clone della repository gprc in gprc/gprc/, e compilerà queste librerie. Inoltre installa protobuf.

#### Setup ambiente per sistemi deprecati
In caso di sistemi vecchi (con gcc < 4.6 ad esempio) è obbligatorio l'utilizzo dell'apposito script di setup:
```
./setup_old_environment.sh
```

Questa modalità è necessaria per la macchina virtuale Oshi VM 8. 
### Compilazione sorgente
Dopo aver configurato l'ambiente per compilare in cpp
```
cd src
make
```

### HelloWorld
Per verificare che il sistema sia operativo, provare a eseguire l'helloworld di grpc.
```
cd src
./greeter_server
```
L'agente, dalla vista di grpc, assumerà infatti il ruolo di server (in quanto espone una porta di rete e esegue i comandi richiesti dal client, nel nostro caso il server python).

Una volta che il server è partito, avviando il corrispettivo helloworld in [server-python](../server-python) si vedrà il programma python restituire la risposta all'HelloWorld inviato dal programma in CPP.

### Esecuzione Agente
```
cd src
./netagent
```

## Tutorial e Riferimenti
E' importante leggere https://github.com/grpc/grpc/blob/master/examples/cpp/cpptutorial.md

## Requisiti Client Agent
### Funzionali
- L'agent deve monitorare gli eventi di rete sul nodo, come malfunzionamenti.
- L'agent deve essere in grado di fornire la configurazione attuale di rete.
- L'agent deve rendere disponibili le informazioni del nodo al server.

### Non Funzionali
- Dato che i nodi da monitorare potrebbero essere semplici switch di rete openflow, è richiesto che l'agent abbia minime dipendenze. Possibilmente il programma deve essere scritto in C++ ed essere distribuito precompilato.
- Per lo stesso motivo del requisito precedente, l'agent non deve impattare sulle prestazioni del nodo in cui è installato.
- L'agent deve comunicare con il nodo server tramite GRPC.
- * Opzionale: L'agent deve essere avviato durante il boot di OSHI.
