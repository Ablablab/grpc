sudo apt-get update
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt-get install gcc-4.8 -y
sudo apt-get install g++-4.8 -y
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 50
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.8 50

sudo apt-get update
sudo apt-get install -y build-essential autoconf libtool
sudo apt-get install -y libgflags-dev libgtest-dev
sudo apt-get install -y clang libc++-dev
sudo apt-get install -y automake curl make g++ unzip
mkdir lib
cd lib
git clone https://github.com/google/protobuf.git
cd protobuf
./autogen.sh
./configure
make check
sudo make install
sudo ldconfig
cd ..
git clone https://github.com/grpc/grpc
cd grpc
git submodule update --init
make -j 4
sudo make install
cd ../../../auth
./generate_auth.sh

