openssl genrsa -passout pass:password -out ca.key 4096
openssl req -passin pass:password -new -x509 -key ca.key -out ca.crt -subj  "/C=IT/ST=Italy/L=Rome/O=NIP/OU=NIP/CN=Root CA"
openssl genrsa -passout pass:password -out agent.key 4096
openssl req -passin pass:password -new -key agent.key -out agent.csr -subj  "/C=IT/ST=Italy/L=Rome/O=NIP/OU=Agent/CN=localhost"
openssl x509 -req -passin pass:password -in agent.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out agent.crt
openssl rsa -passin pass:password -in agent.key -out agent.key
openssl genrsa -passout pass:password -out netserver.key 4096
openssl req -passin pass:password -new -key netserver.key -out netserver.csr -subj  "/C=IT/ST=Italy/L=Rome/O=NIP/OU=NetServer/CN=localhost"
openssl x509 -passin pass:password -req -in netserver.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out netserver.crt
openssl rsa -passin pass:password -in netserver.key -out netserver.key
